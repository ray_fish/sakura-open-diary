FROM python:3.6.10
LABEL maintainer ray.chung
ENV PYTHONUNBUFFERED 1
RUN mkdir /project
RUN mkdir /project/log
WORKDIR /project
COPY . /project
RUN pip install -r requirements.txt
RUN chmod a+x docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]
