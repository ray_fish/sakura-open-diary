import time

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .permissions import *

from diary.models import Diary, DiaryGroup
from diary.serializers import DiarySerializer, DiaryGroupSerializer, DiaryUpdateContentSerializer
from utils.paginations import *


class DiaryView(ModelViewSet):
    permission_classes = (IsAuthenticated, DiaryIsAdminOrOwner)
    serializer_class = DiarySerializer
    queryset = Diary.objects.all()
    pagination_class = MyCursorPagination

    def retrieve(self, request, pk=None, *args, **kwargs):
        try:
            if not Diary.objects.get(id=pk).group.members.filter(id=request.user.id).exists():
                return Response(status=status.HTTP_403_FORBIDDEN)
        except:
            pass
        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = request.data.copy()

        if not request.user.has_perm("diary.create_diary"):
            data['author_id'] = request.user.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        data = request.data
        instance = self.get_object()
        if request.user.has_perm("diary.change_diary"):
            serializer = DiarySerializer(instance, data=data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response({}, status=status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, *args, **kwargs):
        data = request.data
        instance = self.get_object()
        serializer = DiaryUpdateContentSerializer(instance, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response({}, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None, *args, **kwargs):
        try:
            diary = Diary.objects.get(id=pk)

        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        diary.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DiaryGroupView(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = DiaryGroupSerializer
    queryset = DiaryGroup.objects.all()

    def list(self, request, *args, **kwargs):
        if request.user.has_perm("diary.view_diary"):
            return super().list(request, *args, **kwargs)
        else:
            queryset = DiaryGroup.objects.filter(members=request.user)
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

    @action(methods=['get'], detail=True, permission_classes=[DiaryIsAdminOrOwner])
    def diaries(self, request, pk=None, *args, **kwargs):
        try:
            if not DiaryGroup.objects.get(id=pk).group.members.filter(id=request.user.id).exists():
                return Response(status=status.HTTP_403_FORBIDDEN)
        except:
            pass
        paginator = MyCursorPagination()  # 实例化一个分页对象

        # time.sleep(1)
        # TOD O 拿掉測試的延遲
        result = paginator.paginate_queryset(Diary.objects.filter(group_id=pk), request, view=self)
        result_s = DiarySerializer(instance=result, many=True).data
        return paginator.get_paginated_response(result_s)
