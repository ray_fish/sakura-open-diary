from rest_framework import permissions

from . import models

_actions = {
    'POST': 'create',
    'PATCH': 'change',
    'PUT': 'change',
    'DELETE': 'delete',
    'GET': 'view',
    'OPTIONS': 'view'
}


class DiaryGroupIsAdminOrOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj: models.DiaryGroup):
        user = request.user
        if user.has_perm("diary.%s_diary" % _actions[request.method]): return True
        if request.method == "PUT":
            return False
        if user.has_perm("diary.%s_owned_diary" % _actions[request.method]):
            return user in obj.members.all()
        return False


class DiaryIsAdminOrOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj: models.Diary):
        user = request.user
        if request.method not in _actions: return True

        if user.has_perm("diary.%s_diary" % _actions[request.method]): return True

        if user.has_perm("diary.%s_owned_diary" % _actions[request.method]):
            if request.method == "GET":
                return user in obj.group.members.all()
            else:
                return user in obj.group.members.all() and obj.author == user

        return False
