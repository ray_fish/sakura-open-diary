from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models


# from auth.models import User


# Create your models here.
class DiaryGroup(models.Model):
    name = models.CharField(max_length=256)
    members = models.ManyToManyField(get_user_model())

    def __str__(self):
        return self.name


class Diary(models.Model):
    class Meta:
        permissions = (
            ('add_owned_diary', 'Can add diary to owned diary group'),
            ('delete_owned_diary', 'Can delete owned diary'),
            ('change_owned_diary', 'Can change owned diary'),
            ('view_owned_diary', 'Can view diaries that in owned diary group')
        )

    group = models.ForeignKey('DiaryGroup', on_delete=models.CASCADE)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        content = self.content[:5] + "..."
        author = self.author.username
        return f'[{author}] ' + content


