from rest_framework.routers import DefaultRouter

from .views import DiaryView, DiaryGroupView

router = DefaultRouter()
router.register('group', DiaryGroupView)
router.register('', DiaryView)

urlpatterns = [
]
urlpatterns += router.urls
