from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.fields import DateTimeField, IntegerField
from rest_framework.relations import PrimaryKeyRelatedField

from diary.models import Diary, DiaryGroup


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class DiaryGroupSerializer(serializers.ModelSerializer):
    members = UserSerializer(many=True)

    class Meta:
        model = DiaryGroup
        fields = ('id', 'name', 'members')


class DiarySerializer(serializers.ModelSerializer):
    created = DateTimeField(read_only=True)
    group_id = PrimaryKeyRelatedField(write_only=True, many=False, queryset=DiaryGroup.objects)
    author = UserSerializer(read_only=True, many=False, required=False)
    author_id = PrimaryKeyRelatedField(write_only=True, many=False, queryset=User.objects)

    class Meta:
        model = Diary
        # fields = ('id', 'author_id', 'content', 'group_id', 'created')
        fields = ('id', 'author', 'author_id', 'content', 'group_id', 'created')

    def create(self, validated_data):
        author_id = validated_data.pop('author_id')
        group_id = validated_data.pop('group_id')
        validated_data['group'] = group_id
        validated_data['author'] = author_id
        model = self.Meta.model.objects.create(**validated_data)

        return model

    def update(self, instance, validated_data):
        instance.group = validated_data.get('group_id')
        instance.author = validated_data.get('author_id')
        instance.content = validated_data.get('content')
        instance.save()
        return instance


class DiaryUpdateContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diary
        fields = ('content',)

    def update(self, instance, validated_data):
        instance.content = validated_data.get('content')
        instance.save()
        return instance
