from django.contrib import admin

# Register your models here.
from diary.models import Diary, DiaryGroup

@admin.register(Diary)
class DiaryAdmin(admin.ModelAdmin):
    list_filter = ('author', )
    list_per_page = 10

# admin.site.register(Diary)
admin.site.register(DiaryGroup)

