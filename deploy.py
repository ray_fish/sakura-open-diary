#!/usr/bin/env python3
import os
import subprocess
import time

while True:
    try:
        try:
            result = subprocess.check_output(["git", "pull"])
            result = result.decode("utf-8")
            if 'up to date' not in result:
                os.popen('touch uwsgi.ini')
                print("updated!")
                print(result)
        except Exception as e:
            print(e)
        time.sleep(10)
    except KeyboardInterrupt:
        print("bye!")
        exit(0)
