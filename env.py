import os
import socket

ALLOW_HOSTS = None
if 'DATABASE_NAME' not in os.environ:
    import _env

ALLOWED_HOSTS = os.environ["ALLOWED_HOSTS"].split(";")
DATABASE_NAME = os.environ["DATABASE_NAME"]
DATABASE_USER = os.environ["DATABASE_USER"]
DATABASE_PASSWORD = os.environ["DATABASE_PASSWORD"]
# DATABASE_HOST = socket.gethostbyname(os.environ["DATABASE_HOST"])
DATABASE_HOST = os.environ["DATABASE_HOST"]
DATABASE_PORT = os.environ["DATABASE_PORT"]
STATIC_ROOT = os.environ["STATIC_ROOT"]
SECRET_KEY = os.environ["SECRET_KEY"]

RECAPTCHA_PRIVATE_KEY = os.environ["RECAPTCHA_PRIVATE_KEY"]
RECAPTCHA_PUBLIC_KEY = os.environ["RECAPTCHA_PUBLIC_KEY"]