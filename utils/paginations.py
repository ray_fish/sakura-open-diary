from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination


class SmallResultsSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page'


class SmallLimitOffsetPagination(LimitOffsetPagination):
    page_size = 2


class MyCursorPagination(CursorPagination):
    page_size = 2
