#!/bin/sh
(sleep 30 && echo "Start migrating..." && python manage.py migrate --noinput) &
(sleep 10 && echo "Start collect static..." && python manage.py collectstatic --noinput) &
uwsgi --ini uwsgi.ini
